Description
=====
To run the project add those parameters to VM arguments:

For chrome(64+):

-Dbrowser=chrome
-DbaseUrl=url you want to test
-Dwebdriver.chrome.driver=path to chromedriver\chromedriver.exe

For firefox(56+):

-Dbrowser=fierfox
-DbaseUrl=url you want to test
-Dwebdriver.geckodriver.driver=path to geckodriver\geckodriver.exe

To get beautiful and wonderful reports with unicorns(not really) you need to go to Run/Debug Configurations - Listener and and those two
listeners:
TestNgReportsListener
GalenReportingListener

Reports can be found under responsive-ui\target\responsive-ui\Report.html