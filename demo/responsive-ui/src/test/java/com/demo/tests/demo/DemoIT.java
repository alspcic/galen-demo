package com.demo.tests.demo;

import com.demo.tests.helpers.GalenTestBase;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class DemoIT extends GalenTestBase {

    @BeforeSuite
    public void init()
    {
        setSpecLocation("/specs/components/demo/Demo.spec");
        setPageUrl("");
    }

    @Test
    public void desktop_large() throws Throwable
    {
        whenPageIsLoadedOnLargeDesktop();
        thenVerifyTheLayoutForLargeDesktop();
    }

    @Test
    public void desktop_small() throws Throwable
    {
        whenPageIsLoadedOnSmallDesktop();
        thenVerifyTheLayoutForSmallDesktop();
    }

    @Test
    public void tablet() throws Throwable
    {
        whenPageIsLoadedOnTablet();
        thenVerifyTheLayoutForTablet();
    }

    @Test
    public void phablet() throws Throwable
    {
        whenPageIsLoadedOnPhablet();
        thenVerifyTheLayoutForPhablet();
    }

    @Test
    public void mobile_large() throws Throwable
    {
        whenPageIsLoadedOnLargeMobile();
        thenVerifyTheLayoutForLargeMobile();
    }

    @Test
    public void mobile_small() throws Throwable
    {
        whenPageIsLoadedOnSmallMobile();
        thenVerifyTheLayoutForSmallMobile();
    }

}
