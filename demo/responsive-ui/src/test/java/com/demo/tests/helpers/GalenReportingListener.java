package com.demo.tests.helpers;

import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.xml.XmlSuite;

import java.io.IOException;
import java.util.List;

import static com.demo.tests.helpers.GalenConstants.DEFAULT_REPORT_FOLDER_NAME;

public class GalenReportingListener implements IReporter
{

    private final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(this.getClass());

    @Override
    public void generateReport(@NotNull List<XmlSuite> xmlSuites, @NotNull List<ISuite> iSuites, @NotNull String s)
    {
        LOGGER.info("Generating Galen Html report: {}", DEFAULT_REPORT_FOLDER_NAME);

        List<GalenTestInfo> myTests = GalenReportsContainer.getInstance().getAllTests();

        try {
            HtmlReportBuilder myHtmlReportBuilder = new HtmlReportBuilder();
            myHtmlReportBuilder.build(myTests, "target/" + DEFAULT_REPORT_FOLDER_NAME);
        } catch (IOException anIOException) {
            anIOException.printStackTrace();
        }
    }

}
