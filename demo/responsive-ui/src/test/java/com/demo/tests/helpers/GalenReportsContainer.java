package com.demo.tests.helpers;

import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.TestReport;
import com.google.common.collect.Lists;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.List;

public class GalenReportsContainer
{
    private static final GalenReportsContainer theInstance = new GalenReportsContainer();

    private List<GalenTestInfo> theTests = Lists.newLinkedList();

    private final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(this.getClass());

    private GalenReportsContainer() {}

    public static GalenReportsContainer getInstance()
    {
        return theInstance;
    }

    public TestReport registerTest(@NotNull Method aMethod)
    {
        GalenTestInfo myTestInfo = GalenTestInfo.fromMethod(aMethod);
        theTests.add(myTestInfo);
        return myTestInfo.getReport();
    }

    public List<GalenTestInfo> getAllTests()
    {
        return theTests;
    }
}