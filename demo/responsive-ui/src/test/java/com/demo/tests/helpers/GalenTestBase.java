//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.demo.tests.helpers;

import com.galenframework.api.Galen;
import com.galenframework.config.GalenConfig;
import com.galenframework.reports.TestReport;
import com.galenframework.reports.model.LayoutReport;
import com.galenframework.speclang2.pagespec.SectionFilter;
import com.galenframework.utils.GalenUtils;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.FileAssert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.demo.tests.helpers.GalenConstants.ALL;
import static com.demo.tests.helpers.GalenConstants.DEFAULT_SLEEP_TIME;
import static com.demo.tests.helpers.GalenConstants.SPEC_SUFFIX_PATTERN;
import static java.lang.String.format;
import static org.testng.FileAssert.fail;

public abstract class GalenTestBase {
    private final Logger LOGGER = LogManager.getLogger(this.getClass());
    private static ThreadLocal<WebDriver> theDriver = new ThreadLocal();
    private static ThreadLocal<TestReport> theReport = new ThreadLocal();
    private static ThreadLocal<Dimension> theUsedDimension = new ThreadLocal();
    private static final int WIDTH_FOR_SCROLLBAR = 17;
    private Optional<String> theSpecLocation = Optional.absent();
    private Optional<String> thePageUrl = Optional.absent();
    private static final Function<WebDriver, Boolean> pageDocumentReady = (driver) -> {
        return ((JavascriptExecutor)driver).executeScript("return document.readyState", new Object[0]).equals("complete");
    };

    public GalenTestBase() {
    }

    @BeforeMethod
    public void initReport(@NotNull Method method) {
        this.LOGGER.info("ThreadId: {} initializing report for {}", Thread.currentThread().getId(), method);

        theReport.set(GalenReportsContainer.getInstance().registerTest(method));
    }

    @AfterMethod(alwaysRun = true)
    public void closeDriver()
    {
        try
        {
            getDriver().quit();
            Thread.sleep(DEFAULT_SLEEP_TIME);
        }
        catch (Exception anException)
        {
            anException.printStackTrace();
        }
    }

    public void createDriver() {
        this.createDriver(new Dimension(1024, 768));
    }

    public void createDriver(@NotNull Dimension aDimension) {
        this.createDriver(aDimension, false);
    }

    public void createDriver(@NotNull Dimension aDimension, Boolean anOverwriteForChromeOnDesktop) {
        Optional<String> myBrowser = this.getBrowser();
        String myBrowserString = "firefox";
        Optional seleniumGridUrl = null;

        try {
            seleniumGridUrl = this.getSeleniumGridUrl();
        } catch (MalformedURLException var7) {
            var7.printStackTrace();
        }

        DesiredCapabilities myDesiredCapabilities = new DesiredCapabilities();
        if (myBrowser.isPresent()) {
            myBrowserString = (String)myBrowser.get();
        }

        if (myBrowserString.equals("chrome")) {
            myDesiredCapabilities = this.getChromeCapabilities(aDimension, anOverwriteForChromeOnDesktop);
        }

        myDesiredCapabilities.setCapability("browserName", myBrowserString);
        if (seleniumGridUrl.isPresent()) {
            theDriver.set(new RemoteWebDriver((URL)seleniumGridUrl.get(), myDesiredCapabilities));
        } else if (myBrowserString.equals("chrome")) {
            theDriver.set(new ChromeDriver(myDesiredCapabilities));
        } else {
            theDriver.set(new FirefoxDriver(myDesiredCapabilities));
        }

    }

    private DesiredCapabilities getChromeCapabilities(Dimension theDimension) {
        return this.getChromeCapabilities(theDimension, false);
    }

    private DesiredCapabilities getChromeCapabilities(Dimension theDimension, Boolean anOverwriteForChromeOnDesktop) {
        Map myDevice = null;
        DesiredCapabilities myDesiredCapabilities = DesiredCapabilities.chrome();
        if (!anOverwriteForChromeOnDesktop.booleanValue()) {
            switch(theDimension.getWidth()) {
                case 320:
                    myDevice = EmulatedDevice.CHROME_APPLE_IPHONE_5_320X627;
                    break;
                case 375:
                    myDevice = EmulatedDevice.CHROME_APPLE_IPHONE_6_375X627;
                    break;
                case 600:
                    myDevice = EmulatedDevice.CHROME_GOOGLE_NEXUS_7_600X960;
                    break;
                case 768:
                    myDevice = EmulatedDevice.CHROME_APPLE_IPAD_768X1024;
                    break;
                case 1041:
                    myDevice = null;
                    break;
                case 1177:
                    myDevice = null;
                    break;
                default:
                    FileAssert.fail("Incorrect device dimension");
            }
        }

        if (myDevice != null) {
            myDesiredCapabilities.setCapability("goog:chromeOptions", myDevice);
        }

        return myDesiredCapabilities;
    }

    private WebElement findWebElement(@NotNull String anElementLocator) {
        return this.getDriver().findElement(By.cssSelector(anElementLocator));
    }

    public void waitUntilElementIsPresent(@NotNull String anElementLocator, int aMaximumTimeInSeconds) {
        (new WebDriverWait(this.getDriver(), (long)aMaximumTimeInSeconds)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(anElementLocator)));
    }

    public void waitUntilElementIsPresent(@NotNull String anElementLocator) {
        int defaultMaxWaitTime = 6;
        this.waitUntilElementIsPresent(anElementLocator, defaultMaxWaitTime);
    }

    public void whenBrowserIsLaunchedAndUrlEntered(@NotNull String aUrl) {
        try {
            this.getDriver().get(aUrl);
        } catch (UnhandledAlertException var3) {
            ;
        }

    }

    protected void setSpecLocation(@NotNull URL aUrl) {
        this.setSpecLocation(aUrl.getPath());
    }

    protected void setSpecLocation(@NotNull String aPath) {
        this.theSpecLocation = Optional.of(aPath);
    }

    protected void setPageUrl(@NotNull String aUrl) {
        this.thePageUrl = Optional.of(aUrl);
    }

    protected void loadPage(@NotNull Dimension aDimension) throws InterruptedException {
        this.loadPage(aDimension, false);
    }

    protected void loadPage(@NotNull Dimension aDimension, Boolean anOverwriteForChromeOnDesktop) throws InterruptedException {
        try {
            GalenConfig.reloadConfigFromPath("src/test/resources/galen.config");
        } catch (IOException var7) {
            var7.printStackTrace();
        }

        this.createDriver(aDimension, anOverwriteForChromeOnDesktop);
        String url = ((URL)this.getBaseUrl().get()).toString() + (this.thePageUrl.isPresent() ? (String)this.thePageUrl.get() : "");
        this.LOGGER.info("Loading page [{}]", url);
        System.out.println("=============================================================");
        System.out.println("                Running NON-browserstack tests               ");
        System.out.println("=============================================================");
        GalenUtils.resizeDriver(this.getDriver(), aDimension.getWidth(), aDimension.getHeight());
        Thread.sleep(500L);

        this.getDriver().get(url);
        WebDriverWait wait = new WebDriverWait(this.getDriver(), 500L);

        this.LOGGER.info("BaseUrl: " + this.getBaseUrl());
        this.LOGGER.info("ActualUrlL " + this.getDriver().getCurrentUrl());
        wait.until((Function<? super WebDriver, Boolean>) (driver1) -> {
            Object documentReadyState = ((JavascriptExecutor)driver1).executeScript("return document.readyState", new Object[0]);
            return "complete".equals(String.valueOf(documentReadyState));
        });
        theUsedDimension.set(aDimension);
    }

    protected void whenPageIsLoadedOnLargeDesktop() throws InterruptedException {
        this.loadPage(new Dimension(1177, 870));
    }

    protected void whenPageIsLoadedOnSmallDesktop() throws InterruptedException {
        this.loadPage(new Dimension(1041, 768));
    }

    protected void whenPageIsLoadedOnTablet() throws InterruptedException {
        if (System.getProperty("browser").equalsIgnoreCase("firefox")) {
            this.loadPage(new Dimension(785, 1024));
        } else {
            this.loadPage(new Dimension(768, 1024), false);
        }

    }

    protected void whenPageIsLoadedOnChromeInTabletView() throws InterruptedException {
        this.loadPage(new Dimension(785, 1024), true);
    }

    protected void whenPageIsLoadedOnPhablet() throws InterruptedException {
        if (System.getProperty("browser").equalsIgnoreCase("firefox")) {
            this.loadPage(new Dimension(617, 960));
        } else {
            this.loadPage(new Dimension(600, 960), false);
        }

    }

    protected void whenPageIsLoadedOnChromeInPhabletView() throws InterruptedException {
        this.loadPage(new Dimension(617, 960), true);
    }

    protected void whenPageIsLoadedOnLargeMobile() throws InterruptedException {
        if (System.getProperty("browser").equalsIgnoreCase("firefox")) {
            this.loadPage(new Dimension(392, 627));
        } else {
            this.loadPage(new Dimension(375, 627));
        }

    }

    protected void whenPageIsLoadedOnChromeInLargeMobileView() throws InterruptedException {
        this.loadPage(new Dimension(392, 627), true);
    }

    protected void whenPageIsLoadedOnSmallMobile() throws InterruptedException {
        if (System.getProperty("browser").equalsIgnoreCase("firefox")) {
            this.loadPage(new Dimension(337, 480));
        } else {
            this.loadPage(new Dimension(320, 480));
        }

    }

    protected void whenPageIsLoadedOnChromeInSmallMobileView() throws InterruptedException {
        this.loadPage(new Dimension(337, 480), true);
    }

    protected void verifyLayout(@NotNull Dimension aDimension, @NotNull List<String> includedTags) throws InterruptedException {
        this.loadPage(aDimension);
        this.verifyLayout(Optional.of(aDimension), includedTags);
    }

    protected void verifyLayout(@NotNull List<String> includedTags) {
        this.verifyLayout(Optional.absent(), includedTags);
    }

    private void verifyLayout(@NotNull Optional<Dimension> aDimension, @NotNull final List<String> includedTags)
    {
        List<String> mySuffixedTags = null;
        if (!theSpecLocation.isPresent())
        {
            throw new IllegalArgumentException("Undefined spec location. Please use setSpecLocation() before running " +
                    " any test.");
        }

        Dimension dim = aDimension.isPresent() ? aDimension.get() : theUsedDimension.get();
        if (dim.getWidth() >= 1024 + WIDTH_FOR_SCROLLBAR || System.getProperty("browser").equalsIgnoreCase("firefox")) {
            mySuffixedTags = Lists.transform(includedTags, s ->
                    ALL.equalsIgnoreCase(s) ? s : format(SPEC_SUFFIX_PATTERN, s, dim.getWidth() - WIDTH_FOR_SCROLLBAR, dim.getHeight()));
        } else {
            mySuffixedTags = Lists.transform(includedTags, s ->
                    ALL.equalsIgnoreCase(s) ? s : format(SPEC_SUFFIX_PATTERN, s, dim.getWidth(), dim.getHeight()));
        }
        LOGGER.info("Loading tags {}", mySuffixedTags);

        try
        {
            LayoutReport myLayoutReport = Galen.checkLayout(getDriver(), theSpecLocation.get(),
                    new SectionFilter(mySuffixedTags, null), new Properties());
            String myTitle = "Verification using tags: " + Joiner.on(", ").join(mySuffixedTags);

            theReport.get().layout(myLayoutReport, myTitle);

            if (myLayoutReport.errors() > 0)
            {
                fail(myTitle + " Test Failed " + myLayoutReport.getValidationErrorResults());
            }
        }
        catch (IOException e)
        {
            LOGGER.error("Error during UI layout verification.", e);
            throw new UncheckedIOException(e);
        }
    }

    protected void thenVerifyTheLayoutForLargeDesktop() {
        this.verifyLayout(Arrays.asList("desktop", "all"));
    }

    protected void thenVerifyTheLayoutForSmallDesktop() {
        this.verifyLayout(Arrays.asList("desktop", "all"));
    }

    protected void thenVerifyTheLayoutForTablet() {
        this.verifyLayout(Arrays.asList("tablet", "all"));
    }

    protected void thenVerifyTheLayoutForPhablet() {
        this.verifyLayout(Arrays.asList("phablet", "all"));
    }

    protected void thenVerifyTheLayoutForLargeMobile() {
        this.verifyLayout(Arrays.asList("mobile", "all"));
    }

    protected void thenVerifyTheLayoutForSmallMobile() {
        this.verifyLayout(Arrays.asList("mobile", "all"));
    }

    protected Boolean isPageLoaded(@NotNull String aPageTitle) {
        try {
            if (this.waitFor(pageDocumentReady).booleanValue()) {
                return this.getDriver().getTitle().toLowerCase().contains(aPageTitle.toLowerCase());
            }
        } catch (TimeoutException var3) {
            return false;
        }

        return false;
    }

    protected Boolean isElementDisplayed(@NotNull String anElementSelector, @NotNull String aSearchType) throws InterruptedException {
        Function<WebDriver, Boolean> myElement = null;
        String var4 = aSearchType.toLowerCase();
        byte var5 = -1;
        switch(var4.hashCode()) {
            case 3530753:
                if (var4.equals("size")) {
                    var5 = 0;
                }
                break;
            case 113126854:
                if (var4.equals("width")) {
                    var5 = 1;
                }
                break;
            case 566460967:
                if (var4.equals("billboard_height")) {
                    var5 = 3;
                }
                break;
            case 1004743726:
                if (var4.equals("lazy_loading_element")) {
                    var5 = 4;
                }
                break;
            case 1773832825:
                if (var4.equals("mpu_width")) {
                    var5 = 2;
                }
        }

        switch(var5) {
            case 0:
                myElement = (driver) -> {
                    return driver.findElements(By.cssSelector(anElementSelector)).size() > 0;
                };
                break;
            case 1:
                myElement = (driver) -> {
                    return driver.findElement(By.cssSelector(anElementSelector)).getSize().getWidth() > 0;
                };
                break;
            case 2:
                myElement = (driver) -> {
                    return driver.findElement(By.cssSelector(anElementSelector)).getSize().getWidth() > 295;
                };
                break;
            case 3:
                myElement = (driver) -> {
                    return driver.findElement(By.cssSelector(anElementSelector)).getSize().getHeight() > 120;
                };
                break;
            case 4:
                myElement = (driver) -> {
                    return driver.findElements(By.cssSelector(anElementSelector)).size() == 0;
                };
        }

        try {
            if (this.waitFor(myElement).booleanValue()) {
                if (aSearchType.toLowerCase().equals("mpu_width") || aSearchType.toLowerCase().equals("billboard_height") || aSearchType.toLowerCase().equals("lazy_loading_element")) {
                    Thread.sleep(1500L);
                }

                return true;
            }
        } catch (TimeoutException var6) {
            return false;
        }

        return false;
    }

    protected Boolean isElementDisplayed(@NotNull String anElementSelector) throws InterruptedException {
        return this.isElementDisplayed(anElementSelector, "size");
    }

    protected WebElement findElement(@NotNull String anElementSelector) {
        By myCssSelector = By.cssSelector(anElementSelector);
        return this.getDriver().findElement(myCssSelector);
    }

    protected List<WebElement> findElements(@NotNull String anElementSelector) {
        By myCssSelector = By.cssSelector(anElementSelector);
        return this.getDriver().findElements(myCssSelector);
    }

    protected String getElementText(@NotNull String anElementSelector) {
        return this.findElement(anElementSelector).getText();
    }

    protected int getElementSize(@NotNull String anElementSelector) {
        return this.findElements(anElementSelector).size();
    }

    protected void whenElementIsClicked(@NotNull String anElementSelector) {
        this.performActionOnElement(anElementSelector, WebElement::click);
    }

    protected void whenElementTextBoxValueIsSet(@NotNull String anElementSelector, String aTextValue) {
        this.findElement(anElementSelector).sendKeys(new CharSequence[]{aTextValue});
    }

    protected void whenElementTextIsCleared(@NotNull String anElementSelector) {
        this.findElement(anElementSelector).clear();
    }

    protected String whenJavascriptIsExecuted(@NotNull String theScript) {
        return (String)((JavascriptExecutor)this.getDriver()).executeScript(theScript, new Object[0]);
    }

    protected void whenBrowserIsNavigatedBackward() {
        this.getDriver().navigate().back();
    }

    protected void whenBrowserIsRefreshed() {
        this.getDriver().navigate().refresh();
    }

    protected void whenBrowserIsResized(@NotNull Dimension aDimension) {
        this.getDriver().manage().window().setSize(aDimension);
    }

    protected void whenBrowserLocalStorageIsCleared() {
        this.whenJavascriptIsExecuted("window.localStorage.clear();");
    }

    public Boolean verifyAnElementAppears(@NotNull String aLocator) {
        Function<WebDriver, Boolean> myCondition = null;
        myCondition = (driver) -> {
            return driver.findElement(By.cssSelector(aLocator)).isDisplayed();
        };
        return this.waitFor(myCondition);
    }

    public Boolean verifyAllElementsDisappear(@NotNull String aLocator, int aTimeInSeconds) {
        Function<WebDriver, Boolean> myCondition = null;
        myCondition = (driver) -> {
            return driver.findElements(By.cssSelector(aLocator)).size() == 0;
        };
        return this.waitFor(myCondition, aTimeInSeconds);
    }

    public Boolean verifyAllElementsDisappear(@NotNull String aLocator) {
        return this.verifyAllElementsDisappear(aLocator, 15);
    }

    public void scrollTo(@NotNull String anElementSelector) {
        WebElement element = this.getDriver().findElement(By.id(anElementSelector));
        Actions actions = new Actions(this.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    public void whenElementIsScrolledTo(@NotNull String anElementSelector) {
        WebElement element = this.getDriver().findElement(By.cssSelector(anElementSelector));
        Actions actions = new Actions(this.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    public void whenElementIsHovered(@NotNull String anElementSelector) {
        WebElement element = this.getDriver().findElement(By.cssSelector(anElementSelector));
        Actions actions = new Actions(this.getDriver());
        actions.moveToElement(element, 5, 5);
        actions.perform();
    }

    public void whenElementIsClickedAndHolded(@NotNull String anElementSelector) {
        WebElement element = this.getDriver().findElement(By.cssSelector(anElementSelector));
        Actions actions = new Actions(this.getDriver());
        actions.clickAndHold(element);
        actions.perform();
    }

    public String whenCurrentUrlIsRetrieved() {
        return this.getDriver().getCurrentUrl();
    }

    public void whenUrlIsNavigatedTo(@NotNull String theUrl) {
        this.getDriver().navigate().to(theUrl);
    }

    public boolean doesCookieWithSpecificNameExist(@NotNull String aName) {
        return this.getDriver().manage().getCookieNamed(aName) != null;
    }

    public WebDriver getDriver() {
        return (WebDriver)theDriver.get();
    }

    private Optional<URL> getSeleniumGridUrl() throws MalformedURLException {
        String myGridHubUrl = System.getProperty("remote");
        return StringUtils.isBlank(myGridHubUrl) ? Optional.absent() : Optional.of(new URL(myGridHubUrl));
    }

    private Optional<String> getBrowser() {
        String myBrowser = System.getProperty("browser");
        return StringUtils.isBlank(myBrowser) ? Optional.absent() : Optional.of(myBrowser);
    }

    private Optional<URL> getBaseUrl() {
        String myBaseUrl = System.getProperty("baseUrl");

        try {
            return Optional.of(new URL(myBaseUrl));
        } catch (MalformedURLException var3) {
            throw new IllegalStateException("Invalid baseUrl [" + myBaseUrl + "]", var3);
        }
    }

    private void performActionOnElement(@NotNull String anElementSelector, @NotNull Consumer<WebElement> aConsumer) {
        WebElement myElement = this.findElement(anElementSelector);
        if (myElement != null && myElement.isDisplayed()) {
            aConsumer.accept(myElement);
        } else {
            throw new IllegalStateException("Cannot find element " + anElementSelector);
        }
    }

    private Boolean waitFor(@NotNull Function<WebDriver, Boolean> aFunction) {
        return (Boolean)(new FluentWait(this.getDriver())).withTimeout(15L, TimeUnit.SECONDS).pollingEvery(20L, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class).until(aFunction);
    }

    private Boolean waitFor(@NotNull Function<WebDriver, Boolean> aFunction, @NotNull int aTimeInSeconds) {
        return (Boolean)(new FluentWait(this.getDriver())).withTimeout((long)aTimeInSeconds, TimeUnit.SECONDS).pollingEvery(20L, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class).until(aFunction);
    }

    public String getElementCssValue(@NotNull String anElementLocator, @NotNull String aValue) {
        return this.findWebElement(anElementLocator).getCssValue(aValue);
    }

    public String getObjectPropertyFromJS(@NotNull String anObjectName, @NotNull String anPropertyName) {
        String myScript = "return document.querySelector('".concat(anObjectName).concat("').").concat(anPropertyName);
        JavascriptExecutor myJs = (JavascriptExecutor)this.getDriver();
        return String.valueOf(myJs.executeScript(myScript, new Object[0]));
    }
}
