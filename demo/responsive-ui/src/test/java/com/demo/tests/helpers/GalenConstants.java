package com.demo.tests.helpers;

public interface GalenConstants
{

    String ALL = "all";
    String SPEC_SUFFIX_PATTERN = "%s_%dx%d";

    String DEFAULT_REPORT_FOLDER_NAME = "responsive-ui";
    long DEFAULT_SLEEP_TIME = 2000;

}
