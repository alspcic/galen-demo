package com.demo.tests.helpers;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public final class EmulatedDevice
{

    public static Map<String,Map<String,Object>> CHROME_APPLE_IPHONE_5_320X627 = ImmutableMap.of("mobileEmulation",ImmutableMap.of("deviceMetrics", ImmutableMap.of(
            "pixelRatio",2.0,"width",320,"height",568),
            "userAgent","Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25"));

    public static Map<String,Map<String,Object>> CHROME_APPLE_IPHONE_6_375X627 = ImmutableMap.of("mobileEmulation",ImmutableMap.of("deviceMetrics",ImmutableMap.of(
            "pixelRatio",2.0,"width",375,"height",627),"userAgent",
            "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_2 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A4449d Safari/9537.53"));

    public static Map<String,Map<String,Object>> CHROME_GOOGLE_NEXUS_7_600X960 = ImmutableMap.of("mobileEmulation",ImmutableMap.of("deviceMetrics",ImmutableMap.of(
            "pixelRatio",2.0,"width",600,"height",960),"userAgent",
            "Mozilla/5.0 (Linux; U; Android 4.0.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"));

    public static Map<String,Map<String,Object>> CHROME_APPLE_IPAD_768X1024 = ImmutableMap.of("mobileEmulation",ImmutableMap.of("deviceMetrics",ImmutableMap.of(
            "pixelRatio",2.0,"width",768,"height",1024),"userAgent",
            "Mozilla/5.0 (iPad; CPU OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4"));

    public static Map<String,Map<String,Object>> CHROME_WINDOWS_DESKTOP_1024X768 = ImmutableMap.of("mobileEmulation",ImmutableMap.of("deviceMetrics",ImmutableMap.of(
            "pixelRatio",1.0,"width",1024,"height",768),"userAgent",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"));

    public static Map<String,Map<String,Object>> CHROME_WINDOWS_DESKTOP_1160X768 = ImmutableMap.of("mobileEmulation",ImmutableMap.of("deviceMetrics",ImmutableMap.of(
            "pixelRatio",1.0,"width",1160,"height",768),"userAgent",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"));

}

