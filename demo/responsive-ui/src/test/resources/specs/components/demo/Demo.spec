@objects
    galen_framework                 .navbar-brand
    nav_bar                         .nav.navbar-nav
    search                          .navbar-form.navbar-right

    text                            .col-sm-12 h2
    button                          .btn.btn-primary
    envelop_icon                    .fa.fa-envelope.fa-2x
    twitter_icon                    .fa.fa-twitter.fa-2x
    github_icon                     .fa.fa-github-alt.fa-2x
    facebook_icon                   .fa.fa-facebook-square.fa-2x

=    Demo  =
    @on desktop_1160x870, desktop_1024x768
        galen_framework:
            left-of nav_bar -1px
        nav_bar:
            left-of search 136 to 140px
        text:
            above button 20px

        envelop_icon:
            left-of twitter_icon 15px
        twitter_icon:
            left-of github_icon 15px
        github_icon:
            left-of facebook_icon 15px

    @on tablet_768x1024

    @on phablet_600x960, mobile_375x627, mobile_320x480
